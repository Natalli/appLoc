import {ModuleWithProviders} from "@angular/core";
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';
import { LocationEditComponent } from './location-edit/location-edit.component';
import { LocationComponent } from './locathion/location.component';

export const router: Routes = [
    { path: 'category', redirectTo: 'category', pathMatch: 'full'},
    { path: 'edit-category', component: EditCategoryComponent},
    { path: 'location', component: LocationComponent},
    { path: 'location-edit', component: LocationEditComponent},
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);