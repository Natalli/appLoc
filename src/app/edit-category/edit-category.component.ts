import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../category.service';
import {Category} from '../category';
import {AppComponent} from '../app.component';
import {AppService} from '../app.service';

@Component({
    selector: 'app-edit-category',
    templateUrl: './edit-category.component.html',
    styleUrls: ['./edit-category.component.css']
})
export class EditCategoryComponent
extends AppComponent
implements OnInit {
    categories: Category[] = JSON.parse(localStorage.getItem('categories')) ? JSON.parse(localStorage.getItem('categories')) : [];
    currentCategory: any;

    constructor(private categoryService: CategoryService, public appService: AppService) {
        super(appService);
        this.currentCategory = this.categoryService.getCurrentCategory() ? this.categoryService.getCurrentCategory() : {};
    }

    ngOnInit() {
        this.showForm = this.appService.getForm();
    }

    onSubmitAdd() {
        if ( this.currentCategory.id) {
            this.categoryService.editCategory(this.currentCategory);
        } else {
            this.currentCategory.id = (this.categories.length + 1);
            this.categoryService.addCategory(this.currentCategory.id, this.currentCategory.title);
        }
        console.log(this.currentCategory);
        this.changeForm();
        console.log(this.showForm)
    }
}
