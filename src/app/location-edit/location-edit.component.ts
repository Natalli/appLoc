import {Component, OnInit} from '@angular/core';
import {LocationService} from '../location.service';
import {Category} from '../category';
import {CategoryService} from '../category.service';
import {AppComponent} from '../app.component';
import {AppService} from '../app.service';

@Component({
    selector: 'app-location-edit',
    templateUrl: './location-edit.component.html',
    styleUrls: ['./location-edit.component.css']
})
export class LocationEditComponent extends AppComponent implements OnInit {
    categories: Category[];
    locations: Location[] = JSON.parse(localStorage.getItem('locations')) ? JSON.parse(localStorage.getItem('locations')) : [];
    // location: any = {};
    currentLocation: any;

    constructor(private locationService: LocationService,
                private categoryService: CategoryService,
                public appService: AppService) {
        super(appService);
        this.currentLocation = this.locationService.getCurrentLocation() ? this.locationService.getCurrentLocation() : {};
    }

    ngOnInit() {
        this.categories = this.categoryService.getCategory();
        this.showForm = this.appService.getForm();
    }

    onSubmitAdd() {
        if (this.currentLocation.id) {
            this.locationService.editLocation(this.currentLocation);
            console.log(this.currentLocation.title, this.currentLocation.address, this.currentLocation.category_id);
            console.log(this.currentLocation);
        } else {
            this.currentLocation.id = (this.locations.length + 1);
            this.locationService.addLocation(this.currentLocation);
        }
        this.changeForm();
    }
    onMapClick($event) {
        console.log($event);
        let lat = $event.latLng.lat();
        let lng = $event.latLng.lng();
        console.log(lat);
        console.log(lng);
        this.currentLocation.coordinates = lat + ' ' + lng;
        console.log(this.currentLocation.coordinates);
    }

}
