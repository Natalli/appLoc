import {Injectable} from '@angular/core';


@Injectable()
export class AppService {
    showForm = false;

    constructor() {
    }

    getForm() {
        return this.showForm;
    }

    toggleForm() {
        this.showForm = !this.showForm;
        return this.showForm;
    }
}
