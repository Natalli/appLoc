export class Location {
    constructor(public id: number,
                public title: string,
                public address: string,
                public category_id: number,
                public coordinates: number) {
    }
}

