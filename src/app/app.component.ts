import { Component } from '@angular/core';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AppLocation';
  showForm;
    constructor(public appService: AppService) {
        this.showForm = appService.getForm();
    }

    changeForm() {
        this.showForm = this.appService.toggleForm();
        console.log('222', this.showForm)
    }
}
