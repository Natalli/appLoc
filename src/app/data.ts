export const categories = [
    {
        id: 1,
        title: 'Hospital'
    },
    {
        id: 2,
        title: 'School'
    }
];

export const locations = [
    {
        id: 1,
        title: 'Hospital 1',
        address: 'Bunina 13',
        coordinates: '50.465001, 30.621138',
    },
    {
        id: 1,
        title: 'Hospital 2',
        address: 'Bunina 15',
        coordinates: '50.470190, 30.609551',
    },
    {
        id: 2,
        title: 'School 1',
        address: 'Bunina 17',
        coordinates: '50.470190, 30.609551',
    },
    {
        id: 2,
        title: 'School 2',
        address: 'Bunina 19',
        coordinates: '50.470190, 30.609551',
    }
];