import {Component, OnInit, ViewChild} from '@angular/core';
import {Location} from '../location';
import {LocationService} from '../location.service';
import {CategoryService} from '../category.service';
import {AppComponent} from '../app.component';
import {AppService} from '../app.service';


@Component({
    selector: 'app-location',
    templateUrl: 'location.component.html',
    styleUrls: ['location.component.css'],
})
export class LocationComponent extends AppComponent implements OnInit {
    locations: any;
    categories: any;
    category_id: number;
    showForm: boolean;

    constructor(private locationService: LocationService,
                private categoryService: CategoryService,
                public appService: AppService) {
        super(appService);
        this.locations = [];
    }

    ngOnInit() {
        this.locations = this.locationService.getLocation();
        this.categories = this.categoryService.getCategory();
        this.showForm = this.appService.getForm();
        this.locations.sort( function(location1, location2) {
            if ( location1.title < location2.title ){
                return -1;
            }else if( location1.title > location2.title ){
                return 1;
            }else{
                return 0;
            }
        });
    }

    edit(location: Location) {
        this.changeForm();
        this.locationService.setCurrentLocation(location);
    }

    delete(location: Location) {
        this.locationService.deleteLocation(location);
    }
}
